import os
from types import SimpleNamespace

import bec_ipython_client
from bec_ipython_client.main import main_dict
from bec_lib.service_config import ServiceConfig


class Main:
    """This class is the main entry point for the BEC within Jupyter notebook.

    To run BEC within a Jupyter notebook, simply import this module.
    It will automatically import necessary modules and run the BEC client.

    Note:
    The BEC server needs to be running for it to be successful.

    >>> import bec_jupyter.main
    """

    def __init__(self, *args, **kwargs):
        self._ip = get_ipython()

    @classmethod
    def run(cls):
        """Class method to run the main class."""
        main = cls()
        main.bec_startup()

    def bec_startup(self):
        """Method to startup BEC, it runs the bec_startup.py file from the installed BECIpythonClient package."""
        config = ServiceConfig()
        main_dict["config"] = config
        main_dict["args"] = SimpleNamespace(nogui=True, session="")
        main_dict["wait_for_server"] = True
        main_dict["startup_file"] = None

        startup_file = os.path.join(os.path.dirname(bec_ipython_client.__file__), "bec_startup.py")
        with open(startup_file, "r", encoding="utf-8") as file:
            # pylint: disable=exec-used
            exec(file.read(), globals(), locals())
        self.run_magics()
        locals_vars = import_user_modules()
        self.update_user_namespace(locals_vars)

    def update_user_namespace(self, local_vars: dict):
        """Method to update the user namespace in the ipython client."""
        print("-------------------")
        for name, module in local_vars.items():
            print(f"Importing module: {name}.")
            self._ip.user_global_ns[name] = module
        print("-------------------")

    def run_magics(self):
        """Method to run ipython magics."""
        # Enable matplotlib ipympl backend
        self._ip.run_line_magic("matplotlib", "ipympl")
        # Disable jedi due to compatible issues with Ipython in Jupyterlab
        # and IPCompleter.evaluation unsafe for nested autocomplete
        self._ip.run_line_magic("config", "Completer.use_jedi = False")
        self._ip.run_line_magic("config", 'IPCompleter.evaluation="unsafe"')


def import_user_modules() -> dict:
    """Method to additionally import user defined packages and forward their namespace to the ipython client.

    You can add here additional import, but please leave the return statement as it is to ensure proper functionality
    of the startup procedure.
    """
    print("Importing user defined packages")
    import matplotlib.pyplot as plt
    import numpy as np
    from ipywidgets import interact

    return locals()
